::
::
::

if _%COMPUTERNAME%_==_DESKTOP-S9005DS_ goto config_DESKTOP-S9005DS
if _%COMPUTERNAME%_==_ALEX-PC_ goto config_ALEX-PC

echo.
echo ERROR: host '%COMPUTERNAME%' not recognized by PATHS.CMD, build FAILED!
echo You need to add a host dedicated section to PATHS.CMD and retry build.
echo.
goto end

:config_ALEX-PC
set PXE_PATH=E:\PXE
set PATH_TO_VM_DISK="e:\workspace\VMware Virtual Machines\Windows 8.1 x64\myAppDisk.vmdk"
set PATH_TO_VM_TOOLS="c:\Program Files (x86)\VMware\VMware Virtual Disk Development Kit\bin\"
set VOL_MOUNT_LETTER=Q:
set PATH_TO_VIX_TOOLS="c:\Program Files (x86)\VMware\VMware VIX"
set PATH_TO_LOG_FILE="e:\workspace\Projects\MiniHV\logs\w81x64.log"
set PATH_TO_VM_FILE="e:\workspace\VMware Virtual Machines\Windows 8.1 x64\Windows 8.1 x64.vmx"

goto end

:config_DESKTOP-S9005DS

SET PATH_TO_LOG_FILE="E:\HALProject\HAL9000\VM\HAL9000_VM\HAL9000.log"
SET PATH_TO_VM_DISK="E:\HALProject\HAL9000\VM\HAL9000_VM\HAL9000.vmdk"
SET VOL_MOUNT_LETTER="Q:"
SET PXE_PATH="E:\HALProject\HAL9000\PXE"
SET PATH_TO_VM_FILE="E:\HALProject\HAL9000\VM\HAL9000_VM\HAL9000.vmx"
SET PATH_TO_VM_TOOLS="E:\HALProject\HAL9000\tools\VMware Virtual Disk Development Kit\bin"
SET PATH_TO_VIX_TOOLS="E:\HALProject\HAL9000\tools\VMware VIX"
goto end

:end