#pragma once

void
SyscallPreinitSystem(
	void
);

STATUS
SyscallInitSystem(
	void
);

STATUS
SyscallUninitSystem(
	void
);

void
SyscallCpuInit(
	void
);

STATUS
HandleValidateInterface(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleProcessExit(
	void
);

STATUS
HandleProcessCreate(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleProcessGetPid(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleProcessWaitForTermination(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleProcessCloseHandle(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleThreadExit(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleThreadCreate(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleThreadGetTid(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleThreadWaitForTermination(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleThreadCloseHandle(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleFileWrite(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleFileCreate(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleFileRead(
	IN		PQWORD pSyscallParameters
);

STATUS
HandleFileClose(
	IN		PQWORD pSyscallParameters
);