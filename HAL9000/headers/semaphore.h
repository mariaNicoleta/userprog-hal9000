#pragma once

#include "list.h"
#include "synch.h"
#include "mutex.h"

typedef struct _SEMAPHORE
{
	DWORD           Value;
	PMUTEX			Mutex;
} SEMAPHORE, *PSEMAPHORE;

void
SemaphoreInit(
	OUT     PSEMAPHORE      Semaphore,
	IN      DWORD           InitialValue
);

void
SemaphoreDown(
	INOUT   PSEMAPHORE      Semaphore,
	IN      DWORD           Value
);

void
SemaphoreUp(
	INOUT   PSEMAPHORE      Semaphore,
	IN      DWORD           Value
);
