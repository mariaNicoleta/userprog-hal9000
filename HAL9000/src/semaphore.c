#include "HAL9000.h"
#include "thread_internal.h"
#include "semaphore.h"

_No_competing_thread_
void
SemaphoreInit(
	OUT     PSEMAPHORE      Semaphore,
	IN      DWORD           InitialValue
	) 
{
	ASSERT(NULL != Semaphore);

}

//ACQUIRES_EXCL_AND_REENTRANT_LOCK(*Mutex)
//REQUIRES_NOT_HELD_LOCK(*Mutex)
void
SemaphoreDown(
	INOUT   PSEMAPHORE      Semaphore,
	IN      DWORD           Value
)
{

}

//RELEASES_EXCL_AND_REENTRANT_LOCK(*Mutex)
//REQUIRES_EXCL_LOCK(*Mutex)
void
SemaphoreUp(
	INOUT   PSEMAPHORE      Semaphore,
	IN      DWORD           Value
	)
{
    
}