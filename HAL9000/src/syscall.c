#include "HAL9000.h"
#include "syscall.h"
#include "gdtmu.h"
#include "process_internal.h"
#include "process.h"
#include "syscall_defs.h"
#include "syscall_func.h"
#include "syscall_no.h"
#include "dmp_cpu.h"
#include "thread_internal.h"
#include "filesystem.h"
#include "io.h"
#include "hash_table.h"
#include "iomu.h"
//#include "acutils.h"

const char* OurStrStr(const char *str, const char *target);

#define SYSCALL_IF_VERSION_KM                   0x1

_Interlocked_
volatile UM_HANDLE	ProcessHandlesCount = 0;

_Interlocked_
volatile UM_HANDLE	ThreadHandlesCount = 0;

_Interlocked_
volatile UM_HANDLE	FileHandlesCount = 1;

extern void SyscallEntry();

void
SyscallHandler(
	INOUT   PPROCESSOR_STATE    UsermodeProcessorState
)
{
	SYSCALL_ID sysCallId;
	PQWORD pSyscallParameters;
	STATUS status;

	// It is NOT ok to setup the FMASK so that interrupts will be enabled when the system call occurs
	// The issue is that we'll have a user-mode stack and we wouldn't want to receive an interrupt on
	// that stack. This is why we only enable interrupts here.
	ASSERT(CpuIntrGetState() == INTR_OFF);
	CpuIntrSetState(INTR_ON);

	LOG_TRACE_USERMODE("The syscall handler has been called!\n");

	status = STATUS_SUCCESS;
	pSyscallParameters = NULL;

	__try
	{
		if (LogIsComponentTraced(LogComponentUserMode))
		{
			DumpProcessorState(UsermodeProcessorState);
		}

		sysCallId = UsermodeProcessorState->RegisterValues[RegisterR8];
		LOG_TRACE_USERMODE("System call ID is %u\n", sysCallId);

		// The first parameter is the system call ID, we don't care about it => +1
		pSyscallParameters = (PQWORD)UsermodeProcessorState->RegisterValues[RegisterRbp] + 1;

		switch (sysCallId)
		{
		case SyscallIdIdentifyVersion:
			status = HandleValidateInterface(pSyscallParameters);
			break;

			// Processes system calls
		case SyscallIdProcessExit:
			status = HandleProcessExit();
			break;
		case SyscallIdProcessCreate:
			status = HandleProcessCreate(pSyscallParameters);
			break;
		case SyscallIdProcessGetPid:
			status = HandleProcessGetPid(pSyscallParameters);
			break;
		case SyscallIdProcessWaitForTermination:
			status = HandleProcessWaitForTermination(pSyscallParameters);
			break;
		case SyscallIdProcessCloseHandle:
			status = HandleProcessCloseHandle(pSyscallParameters);
			break;

			// Threads system calls
		case SyscallIdThreadCreate:
			status = HandleThreadCreate(pSyscallParameters);
			break;
		case SyscallIdThreadExit:
			status = HandleThreadExit(pSyscallParameters);
			break;
		case SyscallIdThreadGetTid:
			status = HandleThreadGetTid(pSyscallParameters);
			break;
		case SyscallIdThreadWaitForTermination:
			status = HandleThreadWaitForTermination(pSyscallParameters);
			break;
		case SyscallIdThreadCloseHandle:
			status = HandleThreadCloseHandle(pSyscallParameters);
			break;

			// Files system calls
		case SyscallIdFileCreate:
			status = HandleFileCreate(pSyscallParameters);
			break;
		case SyscallIdFileRead:
			status = HandleFileRead(pSyscallParameters);
			break;
		case SyscallIdFileWrite:
			status = HandleFileWrite(pSyscallParameters);
			break;
		case SyscallIdFileClose:
			status = HandleFileClose(pSyscallParameters);
			break;

		default:
			break;
		}
	}
	__finally
	{
		LOG_TRACE_USERMODE("Will set UM RAX to 0x%x\n", status);

		UsermodeProcessorState->RegisterValues[RegisterRax] = status;

		CpuIntrSetState(INTR_OFF);
	}
}

STATUS
HandleValidateInterface(
	IN		PQWORD pSyscallParameters
)
{
	SYSCALL_IF_VERSION version = (SYSCALL_IF_VERSION)pSyscallParameters[0];
	if (version == SYSCALL_IF_VERSION_KM)
	{
		return STATUS_SUCCESS;
	}
	else
	{
		return STATUS_ASSERTION_FAILURE;
	}
}

STATUS
HandleProcessExit()
{
	PPROCESS				process = GetCurrentProcess();

	ProcessTerminate(process);
	
	return STATUS_SUCCESS;
}

STATUS
HandleProcessCreate(
	IN		PQWORD pSyscallParameters
)
{
	char*                   processPath = (char*)pSyscallParameters[0];
	DWORD                   pathLength = (DWORD)pSyscallParameters[1];
	char*                   arguments = (char*)pSyscallParameters[2];
	DWORD                   argLength = (DWORD)pSyscallParameters[3];
	UM_HANDLE*              processHandle = (UM_HANDLE*)pSyscallParameters[4];

	if ((strlen(processPath) + 1) != pathLength || (strlen(arguments) + 1) != argLength)
	{
		LOG("[MARIA] Path length is incorrect\n");
		return STATUS_NO_DATA_AVAILABLE;
	}

	const char* systemPath = IomuGetSystemPartitionPath();
	char absolutePath[MAX_PATH];
	LOG("[MARIA] Process path is %s\n", processPath);
	if (OurStrStr(processPath, systemPath) == NULL)
	{
		snprintf(absolutePath, MAX_PATH, "%sAPPLIC~1\\%s", systemPath, processPath);
	}
	else
	{
		strcpy(absolutePath, processPath);
	}

	LOG("[MARIA] Absolute path is %s\n", absolutePath);

	PPROCESS process = NULL;
	STATUS createStatus = ProcessCreate(absolutePath, arguments, &process);
	if (!SUCCEEDED(createStatus))
	{
		LOG("[MARIA] Process create function failed\n");
		return createStatus;
	}
	if (process == NULL)
	{
		LOG("[MARIA] Process is null\n");
		return STATUS_INTERNAL_ERROR;
	}

	PHASH_STRUCT_PROCESS pProcessHandleStruct = ExAllocatePoolWithTag(0, sizeof(HASH_STRUCT_PROCESS), HEAP_TEST_TAG, 0);
	if (pProcessHandleStruct == NULL)
	{
		LOG("Process hash structure creation error!\n");
		return STATUS_HEAP_INSUFFICIENT_RESOURCES;
	}
	pProcessHandleStruct->Process = process;
	pProcessHandleStruct->ProcessHandle = _InterlockedIncrement((DWORD *) &ProcessHandlesCount);

	INTR_STATE oldStateProcessLock;
	PLOCK processLock = &GetCurrentProcess()->ProcessHandlesLock;
	LockAcquire(processLock, &oldStateProcessLock);
	HashTableInsert(&GetCurrentProcess()->ProcessHandles, &pProcessHandleStruct->HashEntry);
	LockRelease(processLock, oldStateProcessLock);

	PHASH_STRUCT_FILE pFileHandleStruct = ExAllocatePoolWithTag(0, sizeof(HASH_STRUCT_FILE), HEAP_TEST_TAG, 0);
	if (pFileHandleStruct == NULL)
	{
		LOG("STDOUT hash structure creation error!\n");
		return STATUS_NO_DATA_AVAILABLE;
	}
	pFileHandleStruct->FileHandle = UM_FILE_HANDLE_STDOUT;

	INTR_STATE oldStateFilesLock;
	PLOCK filesLock = &process->FileHandlesLock;
	LockAcquire(filesLock, &oldStateFilesLock);
	HashTableInsert(&process->FileHandles, &pFileHandleStruct->HashEntry);
	LockRelease(filesLock, oldStateFilesLock);

	*processHandle = pProcessHandleStruct->ProcessHandle;

	return STATUS_SUCCESS;
}

STATUS
HandleProcessGetPid(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE               processHandle = (UM_HANDLE)pSyscallParameters[0];
	PID*                    processId = (PID*)pSyscallParameters[1];
	
	if (UM_INVALID_HANDLE_VALUE == processHandle)
	{
		processId = &(GetCurrentProcess()->Id);
		return STATUS_SUCCESS;
	}

	INTR_STATE oldState;
	PLOCK processLock = &GetCurrentProcess()->ProcessHandlesLock;
	LockAcquire(processLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->ProcessHandles, (PHASH_KEY) &processHandle);
	LockRelease(processLock, oldState);

	if (hashTableEntry == NULL)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_PROCESS pProcess = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_PROCESS, HashEntry);
	*processId = pProcess->Process->Id;
	
	return STATUS_SUCCESS;
}

STATUS
HandleProcessWaitForTermination(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE               processHandle = (UM_HANDLE)pSyscallParameters[0];
	STATUS*                 terminationStatus = (STATUS*)pSyscallParameters[1];
	
	INTR_STATE oldState;
	PLOCK processLock = &GetCurrentProcess()->ProcessHandlesLock;
	LockAcquire(processLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->ProcessHandles, (PHASH_KEY) &processHandle);
	LockRelease(processLock, oldState);

	if (hashTableEntry == NULL)
	{
		LOG("[MARIA] HandleProcessWaitForTermination - Hash table entry is null!\n");
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_PROCESS pProcess = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_PROCESS, HashEntry);

	ProcessWaitForTermination(pProcess->Process, terminationStatus);

	return STATUS_SUCCESS;
}

STATUS
HandleProcessCloseHandle(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE               processHandle = (UM_HANDLE)pSyscallParameters[0];

	INTR_STATE oldState;
	PLOCK processLock = &GetCurrentProcess()->ProcessHandlesLock;
	LockAcquire(processLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->ProcessHandles, (PHASH_KEY) &processHandle);
	LockRelease(processLock, oldState);

	if (hashTableEntry == NULL)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_PROCESS pProcess = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_PROCESS, HashEntry);

	ProcessCloseHandle(pProcess->Process);

	LockAcquire(processLock, &oldState);
	HashTableRemove(&GetCurrentProcess()->ProcessHandles, (PHASH_KEY) &processHandle);
	LockRelease(processLock, oldState);
	
	LOG("Returning success from processclosehandle\n");
	return STATUS_SUCCESS;
}

STATUS
HandleThreadExit(
	IN		PQWORD pSyscallParameters
)
{
	STATUS					exitStatus = (STATUS)pSyscallParameters[0];

	LOG("handling thread exit\n");
	ThreadExit(exitStatus);
	return STATUS_SUCCESS;
}

STATUS
HandleThreadCreate(
	IN		PQWORD pSyscallParameters
)
{	
	PFUNC_ThreadStart       startFunction = (PFUNC_ThreadStart)pSyscallParameters[0];
	PVOID                   context = (PVOID)pSyscallParameters[1];
	UM_HANDLE*              threadHandle = (UM_HANDLE*)pSyscallParameters[2];

	PTHREAD createdThread = NULL;
	STATUS status = ThreadCreateEx(
		"New thread",
		ThreadPriorityDefault,
		startFunction,
		context,
		&createdThread,
		GetCurrentProcess()
	);

	if (!SUCCEEDED(status))
	{
		LOG_FUNC_ERROR("[MARIA] Thread creation error %s!", status);
		return status;
	}

	//add in hash table
	PHASH_STRUCT_THREAD pThreadHandleStruct = ExAllocatePoolWithTag(0, sizeof(HASH_STRUCT_THREAD), HEAP_TEST_TAG, 0);
	if (pThreadHandleStruct == NULL)
	{
		status = STATUS_HEAP_INSUFFICIENT_RESOURCES;
		LOG_FUNC_ERROR("[MARIA] Thread hash structure creation error %s!", status);
		return status;
	}
	pThreadHandleStruct->Thread = createdThread;
	pThreadHandleStruct->ThreadHandle = _InterlockedIncrement((DWORD *) &ThreadHandlesCount);

	INTR_STATE oldStateThreadsLock;
	PLOCK threadsLock = &GetCurrentProcess()->ThreadHandlesLock;
	LockAcquire(threadsLock, &oldStateThreadsLock);
	HashTableInsert(&GetCurrentProcess()->ThreadHandles, &pThreadHandleStruct->HashEntry);
	LockRelease(threadsLock, oldStateThreadsLock);

	*threadHandle = pThreadHandleStruct->ThreadHandle;

	return status;
}

STATUS
HandleThreadGetTid(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE               threadHandle = (UM_HANDLE)pSyscallParameters[0];
	TID*                    threadId = (TID*)pSyscallParameters[1];
	
	if (UM_INVALID_HANDLE_VALUE == threadHandle)
	{
		threadId = &(GetCurrentThread()->Id);
		return STATUS_SUCCESS;
	}

	INTR_STATE oldState;
	PLOCK threadsLock = &GetCurrentProcess()->ThreadHandlesLock;
	LockAcquire(threadsLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->ThreadHandles, (PHASH_KEY)&threadHandle);
	LockRelease(threadsLock, oldState);

	if (hashTableEntry == NULL)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_THREAD pThread = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_THREAD, HashEntry);
	*threadId = pThread->Thread->Id;

	return STATUS_SUCCESS;
}

STATUS
HandleThreadWaitForTermination(
	IN		PQWORD pSyscallParameters
)
{
	UM_HANDLE               threadHandle = (UM_HANDLE)pSyscallParameters[0];
	STATUS*                 terminationStatus = (STATUS*)pSyscallParameters[1];

	PTHREAD thread = NULL;
	if (UM_INVALID_HANDLE_VALUE == threadHandle)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}

	INTR_STATE oldState;
	PLOCK threadsLock = &GetCurrentProcess()->ThreadHandlesLock;
	LockAcquire(threadsLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->ThreadHandles, (PHASH_KEY)&threadHandle);
	LockRelease(threadsLock, oldState);

	if (hashTableEntry == NULL)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_THREAD pThread = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_THREAD, HashEntry);
	thread = pThread->Thread;

	ThreadWaitForTermination(thread, terminationStatus);
	return STATUS_SUCCESS;
}

STATUS
HandleThreadCloseHandle(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE               threadHandle = (UM_HANDLE)pSyscallParameters[0];

	INTR_STATE oldState;
	PLOCK threadsLock = &GetCurrentProcess()->ThreadHandlesLock;
	LockAcquire(threadsLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->ThreadHandles, (PHASH_KEY) &threadHandle);
	LockRelease(threadsLock, oldState);

	if (hashTableEntry == NULL)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_THREAD pThread = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_THREAD, HashEntry);

	ThreadCloseHandle(pThread->Thread);

	LockAcquire(threadsLock, &oldState);
	HashTableRemove(&GetCurrentProcess()->ThreadHandles, (PHASH_KEY) &threadHandle);
	LockRelease(threadsLock, oldState);

	return STATUS_SUCCESS;
}

STATUS
HandleFileWrite(
	IN		PQWORD pSyscallParameters
)
{
	UM_HANDLE				fileHandle = (UM_HANDLE)pSyscallParameters[0];
	PVOID					buffer = (PVOID)pSyscallParameters[1];
	QWORD					bytesToWrite = (QWORD)pSyscallParameters[2];
	QWORD*					bytesWritten = (QWORD*)pSyscallParameters[3];

	if (UM_FILE_HANDLE_STDOUT == fileHandle)
	{
		LOG("[%s]:[%s]\n", ProcessGetName(NULL), buffer);
		*bytesWritten = bytesToWrite;
		return STATUS_SUCCESS;
	}
	else
	{
		INTR_STATE oldState;
		PLOCK filesLock = &GetCurrentProcess()->FileHandlesLock;
		LockAcquire(filesLock, &oldState);
		PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->FileHandles, (PHASH_KEY)&fileHandle);
		LockRelease(filesLock, oldState);
		if (hashTableEntry == NULL)
		{
			LOG("File handle %s is closed!\n", fileHandle);
			return STATUS_NO_DATA_AVAILABLE;
		}
		return STATUS_SUCCESS;
	}
}

STATUS
HandleFileCreate(
	IN		PQWORD pSyscallParameters
)
{	
	char*                   path = (char*)pSyscallParameters[0];
	DWORD                   pathLength = (DWORD)pSyscallParameters[1];
	BOOLEAN                 directory = (BOOLEAN)pSyscallParameters[2];
	BOOLEAN                 create = (BOOLEAN)pSyscallParameters[3];
	UM_HANDLE*              fileHandle = (UM_HANDLE*)pSyscallParameters[4];

	if (pathLength != (strlen(path) + 1))
	{
		return STATUS_NO_DATA_AVAILABLE;
	}

	// validate path
	const char* systemPath = IomuGetSystemPartitionPath();
	char* absolutePath = NULL;
	if (OurStrStr(path, systemPath) == NULL)
	{
		sprintf(absolutePath, "%s/APPLIC~1/%s", systemPath, path);
	}
	else
	{
		absolutePath = path;
	}

	PFILE_OBJECT file = NULL;
	STATUS createFileStatus = IoCreateFile(&file, absolutePath, directory, create, FALSE);
	if (!SUCCEEDED(createFileStatus))
	{
		return createFileStatus;
	}

	PHASH_STRUCT_FILE pFileHandleStruct = ExAllocatePoolWithTag(0, sizeof(HASH_STRUCT_FILE), HEAP_TEST_TAG, 0);
	if (pFileHandleStruct == NULL)
	{
		LOG("File hash structure creation error!\n");
		return STATUS_NO_DATA_AVAILABLE;
	}
	pFileHandleStruct->File = file;
	pFileHandleStruct->FileHandle = _InterlockedIncrement((DWORD *) &FileHandlesCount);

	INTR_STATE oldStateFilesLock;
	PLOCK filesLock = &GetCurrentProcess()->FileHandlesLock;
	LockAcquire(filesLock, &oldStateFilesLock);
	HashTableInsert(&GetCurrentProcess()->FileHandles, &pFileHandleStruct->HashEntry);
	LockRelease(filesLock, oldStateFilesLock);

	*fileHandle = pFileHandleStruct->FileHandle;
	
	return STATUS_SUCCESS;
}

STATUS
HandleFileRead(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE               fileHandle = (UM_HANDLE)pSyscallParameters[0];
	PVOID                   buffer = (PVOID)pSyscallParameters[1];
	QWORD                   bytesToRead = (QWORD)pSyscallParameters[2];
	QWORD*                  bytesRead = (QWORD*)pSyscallParameters[3];

	INTR_STATE oldState;
	PLOCK filesLock = &GetCurrentProcess()->FileHandlesLock;
	LockAcquire(filesLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->FileHandles, (PHASH_KEY) &fileHandle);
	LockRelease(filesLock, oldState);

	if (hashTableEntry == NULL)
	{
		LOG("File handle is closed!!!\n");
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_FILE pFile = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_FILE, HashEntry);

	return IoReadFile(pFile->File, bytesToRead, NULL, buffer, bytesRead);
}

STATUS
HandleFileClose(
	IN		PQWORD pSyscallParameters
)
{	
	UM_HANDLE             fileHandle = (UM_HANDLE)pSyscallParameters[0];

	INTR_STATE oldState;
	PLOCK filesLock = &GetCurrentProcess()->FileHandlesLock;
	LockAcquire(filesLock, &oldState);
	PHASH_ENTRY hashTableEntry = HashTableLookup(&GetCurrentProcess()->FileHandles, (PHASH_KEY) &fileHandle);
	LockRelease(filesLock, oldState);

	if (hashTableEntry == NULL)
	{
		return STATUS_NO_DATA_AVAILABLE;
	}
	PHASH_STRUCT_FILE pFile = CONTAINING_RECORD(hashTableEntry, HASH_STRUCT_FILE, HashEntry);

	STATUS closeFileStatus = IoCloseFile(pFile->File);
	if (!SUCCEEDED(closeFileStatus))
	{
		return closeFileStatus;
	}

	LockAcquire(filesLock, &oldState);
	HashTableRemove(&GetCurrentProcess()->FileHandles, (PHASH_KEY) &fileHandle);
	LockRelease(filesLock, oldState);
	
	return STATUS_SUCCESS;
}

void
SyscallPreinitSystem(
	void
)
{

}

STATUS
SyscallInitSystem(
	void
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallUninitSystem(
	void
)
{
	return STATUS_SUCCESS;
}

void
SyscallCpuInit(
	void
)
{
	IA32_STAR_MSR_DATA starMsr;
	WORD kmCsSelector;
	WORD umCsSelector;

	memzero(&starMsr, sizeof(IA32_STAR_MSR_DATA));

	kmCsSelector = GdtMuGetCS64Supervisor();
	ASSERT(kmCsSelector + 0x8 == GdtMuGetDS64Supervisor());

	umCsSelector = GdtMuGetCS32Usermode();
	/// DS64 is the same as DS32
	ASSERT(umCsSelector + 0x8 == GdtMuGetDS32Usermode());
	ASSERT(umCsSelector + 0x10 == GdtMuGetCS64Usermode());

	// Syscall RIP <- IA32_LSTAR
	__writemsr(IA32_LSTAR, (QWORD)SyscallEntry);

	LOG_TRACE_USERMODE("Successfully set LSTAR to 0x%X\n", (QWORD)SyscallEntry);

	// Syscall RFLAGS <- RFLAGS & ~(IA32_FMASK)
	__writemsr(IA32_FMASK, RFLAGS_INTERRUPT_FLAG_BIT);

	LOG_TRACE_USERMODE("Successfully set FMASK to 0x%X\n", RFLAGS_INTERRUPT_FLAG_BIT);

	// Syscall CS.Sel <- IA32_STAR[47:32] & 0xFFFC
	// Syscall DS.Sel <- (IA32_STAR[47:32] + 0x8) & 0xFFFC
	starMsr.SyscallCsDs = kmCsSelector;

	// Sysret CS.Sel <- (IA32_STAR[63:48] + 0x10) & 0xFFFC
	// Sysret DS.Sel <- (IA32_STAR[63:48] + 0x8) & 0xFFFC
	starMsr.SysretCsDs = umCsSelector;

	__writemsr(IA32_STAR, starMsr.Raw);

	LOG_TRACE_USERMODE("Successfully set STAR to 0x%X\n", starMsr.Raw);
}


const char* OurStrStr(const char *str, const char *target) 
{
	if (!*target) return str;
	char *p1 = (char*)str;
	while (*p1) {
		char *p1Begin = p1, *p2 = (char*)target;
		while (*p1 && *p2 && *p1 == *p2) {
			p1++;
			p2++;
		}
		if (!*p2)
			return p1Begin;
		p1 = p1Begin + 1;
	}
	return NULL;
}

