#include "HAL9000.h"
#include "ex_timer.h"
#include "iomu.h"
#include "thread_internal.h"

STATUS
ExTimerInit(
    OUT     PEX_TIMER       Timer,
    IN      EX_TIMER_TYPE   Type,
    IN      QWORD           Time
    )
{
    STATUS status;

    if (NULL == Timer)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (Type > ExTimerTypeMax)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;

    memzero(Timer, sizeof(EX_TIMER));

    Timer->Type = Type;
    if (Timer->Type != ExTimerTypeAbsolute)
    {
        // relative time

        // if the time trigger time has already passed the timer will
        // be signaled after the first scheduler tick
        Timer->TriggerTimeUs = IomuGetSystemTimeUs() + Time;
        Timer->ReloadTimeUs = Time;
    }
    else
    {
        // absolute
        Timer->TriggerTimeUs = Time;
    }

	ExEventInit(&Timer->TimerEvent, ExEventTypeNotification, FALSE);

	LIST_ENTRY* SystemTimersHead = &getTimerSystemData()->TimersListHead;
	ASSERT(SystemTimersHead != NULL);

	LOCK* timerLock = &getTimerSystemData()->TimersLock;
	INTR_STATE oldState;

	LockAcquire(timerLock, &oldState);
	InsertOrderedList(SystemTimersHead, &Timer->TimerList, ListCompareTimers);
	LOGPL("Timer %d has been added to the list of timers\n", Timer);
	LockRelease(timerLock, oldState);

    return status;
}

void
ExTimerStart(
    IN      PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    if (Timer->TimerUninited)
    {
        return;
    }

    Timer->TimerStarted = TRUE;
}

void
ExTimerStop(
    IN      PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    if (Timer->TimerUninited)
    {
        return;
    }

	ExEventClearSignal(&Timer->TimerEvent);
    Timer->TimerStarted = FALSE;
}

void
ExTimerWait(
    INOUT   PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    if (Timer->TimerUninited)
    {
        return;
    }

	LOGPL("Timer %d is waiting for signal\n", Timer);
	ExEventWaitForSignal(&Timer->TimerEvent);

    /*while (IomuGetSystemTimeUs() < Timer->TriggerTimeUs && Timer->TimerStarted)
    {
        ThreadYield();
    }*/
}

void
ExTimerUninit(
    INOUT   PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    ExTimerStop(Timer);

    Timer->TimerUninited = TRUE;

	LOCK* timerLock = &getTimerSystemData()->TimersLock;
	INTR_STATE oldState;

	LockAcquire(timerLock, &oldState);
	RemoveEntryList(&Timer->TimerList);
	LockRelease(timerLock, oldState);
}

INT64
ExTimerCompareTimers(
    IN      PEX_TIMER     FirstElem,
    IN      PEX_TIMER     SecondElem
)
{
    return FirstElem->TriggerTimeUs - SecondElem->TriggerTimeUs;
}

INT64
ListCompareTimers(
	IN      PLIST_ENTRY   FirstElem,
	IN      PLIST_ENTRY   SecondElem
)
{
	ASSERT(FirstElem != NULL);
	ASSERT(SecondElem != NULL);
	PEX_TIMER firstTimer = CONTAINING_RECORD(FirstElem, EX_TIMER, TimerList);
	PEX_TIMER secondTimer = CONTAINING_RECORD(SecondElem, EX_TIMER, TimerList);
	return ExTimerCompareTimers(firstTimer, secondTimer);
}