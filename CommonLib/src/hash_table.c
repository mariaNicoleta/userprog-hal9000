#include "common_lib.h"
#include "hash_table.h"

#pragma warning(push)

// warning C4200: nonstandard extension used: zero-sized array in struct/union
#pragma warning(disable:4200)
typedef struct _HASH_TABLE_DATA
{
    HASH_ENTRY          Entries[0];
} HASH_TABLE_DATA, *PHASH_TABLE_DATA;
#pragma warning(pop)

static
__forceinline
PHASH_KEY
_HashTableObtainKeyAddress(
    IN      PHASH_TABLE         HashTable,
    IN      PHASH_ENTRY         Element
    )
{
    ASSERT(HashTable != NULL);
    ASSERT(Element != NULL);

    return (PHASH_KEY) ((PBYTE)Element + HashTable->OffsetToKey);
}

DWORD
HashTablePreinit(
    OUT     PHASH_TABLE         HashTable,
    IN      DWORD               MaxKeys,
    IN      DWORD               KeySize
    )
{
    ASSERT(HashTable != NULL);

    memzero(HashTable, sizeof(HASH_TABLE));

    HashTable->MaxKeys = MaxKeys;
    HashTable->KeySize = KeySize;

    ASSERT(HashTable->MaxKeys < MAX_DWORD / sizeof(HASH_ENTRY));

    return HashTable->MaxKeys * sizeof(HASH_ENTRY);
}

void
HashTableInit(
    INOUT   PHASH_TABLE         HashTable,
    IN      PHASH_TABLE_DATA    TableData,
    IN      PFUNC_HashFunction  HashFunction,
    IN      INT32               OffsetToKey
    )
{
    ASSERT(HashTable != NULL);

    HashTable->TableData = TableData;
    HashTable->HashFunc = HashFunction;
    HashTable->OffsetToKey = OffsetToKey;

    for (DWORD i = 0; i < HashTable->MaxKeys; ++i)
    {
        InitializeListHead(&HashTable->TableData->Entries[i]);
    }
}

void
HashTableClear(
    INOUT   PHASH_TABLE         HashTable,
    IN_OPT  PFUNC_FreeFunction  FreeFunction,
    IN_OPT  PVOID               FreeContext
    )
{
    HASH_ITERATOR it;
    PHASH_ENTRY pEntry;

    ASSERT(HashTable != NULL);

    HashTableIteratorInit(HashTable, &it);

    while ((pEntry = HashTableIteratorNext(&it)) != NULL)
    {
        // remove from hash table
        HashTableRemoveEntry(HashTable, pEntry);

        // call free function if one was provided
        if (FreeFunction != NULL)
        {
            FreeFunction(pEntry, FreeContext);
        }
    }
}

DWORD
HashTableSize(
    IN      PHASH_TABLE         HashTable
    )
{
    ASSERT(HashTable != NULL);

    return HashTable->NumberOfElements;
}

void
HashTableInsert(
    INOUT   PHASH_TABLE         HashTable,
    OUT     PHASH_ENTRY         Element
    )
{
    PHASH_KEY pKey;
    QWORD hIndex;

    ASSERT(HashTable != NULL);
    ASSERT(Element != NULL);

    pKey = _HashTableObtainKeyAddress(HashTable, Element);

    hIndex = HashTable->HashFunc(pKey,
                                 HashTable->KeySize,
                                 HashTable->MaxKeys);
    ASSERT(hIndex < HashTable->MaxKeys);

    InsertTailList(&HashTable->TableData->Entries[hIndex],
                   Element);

    HashTable->NumberOfElements++;
}

PTR_SUCCESS
PHASH_ENTRY
HashTableRemove(
    INOUT   PHASH_TABLE         HashTable,
    IN      PHASH_KEY           Key
    )
{
    PHASH_ENTRY pHashEntry;

    pHashEntry = HashTableLookup(HashTable, Key);
    if (pHashEntry != NULL)
    {
        HashTableRemoveEntry(HashTable, pHashEntry);
    }

    return pHashEntry;
}

void
HashTableRemoveEntry(
    INOUT   PHASH_TABLE         HashTable,
    IN      PHASH_ENTRY         Element
    )
{
    ASSERT(HashTable != NULL);
    ASSERT(Element != NULL);

    RemoveEntryList(Element);

    HashTable->NumberOfElements--;
}

PTR_SUCCESS
PHASH_ENTRY
HashTableLookup(
    INOUT   PHASH_TABLE         HashTable,
    IN      PHASH_KEY           Key
    )
{
    QWORD hIndex;
    PHASH_ENTRY pResult;

    ASSERT(HashTable != NULL);
    ASSERT(Key != NULL);

    pResult = NULL;
    hIndex = HashTable->HashFunc(Key,
                                 HashTable->KeySize,
                                 HashTable->MaxKeys);

    for (HASH_ENTRY* pCurrentEntry = HashTable->TableData->Entries[hIndex].Flink;
         pCurrentEntry != &HashTable->TableData->Entries[hIndex];
         pCurrentEntry = pCurrentEntry->Flink)
    {
        PHASH_KEY pCurrentKey = _HashTableObtainKeyAddress(HashTable,
                                                           pCurrentEntry);

        if (memcmp(Key, pCurrentKey, HashTable->KeySize) == 0)
        {
            pResult = pCurrentEntry;
            break;
        }
    }

    return pResult;
}

void
HashTableIteratorInit(
    INOUT   PHASH_TABLE         HashTable,
    OUT     PHASH_ITERATOR      HashIterator
    )
{
    ASSERT(HashTable != NULL);
    ASSERT(HashIterator != NULL);

    HashIterator->HashTable = HashTable;
    HashIterator->KeyIndex = 0;
    HashIterator->CurrentEntry = HashTable->TableData->Entries[0].Flink;
}

PHASH_ENTRY
HashTableIteratorNext(
    INOUT   PHASH_ITERATOR      HashIterator
    )
{
    PHASH_ENTRY pResult;

    ASSERT(HashIterator != NULL);

    pResult = NULL;

    while (HashIterator->KeyIndex < HashIterator->HashTable->MaxKeys)
    {
        while (HashIterator->CurrentEntry != &HashIterator->HashTable->TableData->Entries[HashIterator->KeyIndex])
        {
            pResult = HashIterator->CurrentEntry;

            // advance current entry
            HashIterator->CurrentEntry = HashIterator->CurrentEntry->Flink;

            break;
        }

        if (pResult)
        {
            break;
        }

        HashIterator->KeyIndex++;
        HashIterator->CurrentEntry = HashIterator->HashTable->TableData->Entries[HashIterator->KeyIndex].Flink;
    }

    return pResult;
}

QWORD
(__cdecl HashFuncGenericIncremental) (
    IN_READS_BYTES(KeyLength)   PHASH_KEY   Key,
    IN                          DWORD       KeyLength,
    IN                          DWORD       MaxKeys
    )
{
    QWORD keyValue;

    ASSERT(KeyLength <= MAX_QWORD);

    keyValue = 0;
    memcpy(&keyValue, Key, KeyLength);

    return keyValue % MaxKeys;
}

#define HASH_UNIVERSAL_A        1701
#define HASH_UNIVERSAL_B        59
#define HASH_UNIVERSAL_P        10007

QWORD
(__cdecl HashFuncUniversal) (
    IN_READS_BYTES(KeyLength)   PHASH_KEY   Key,
    IN                          DWORD       KeyLength,
    IN                          DWORD       MaxKeys
    )
{
    QWORD keyValue;
    QWORD result;

    ASSERT(KeyLength <= MAX_QWORD);

    keyValue = 0;
    memcpy(&keyValue, Key, KeyLength);

    // perform modulo operation first to make it less likely for
    // an overflow to happen
    result = keyValue % HASH_UNIVERSAL_P;

    ASSERT(HASH_UNIVERSAL_A < MAX_QWORD / result - HASH_UNIVERSAL_B);

    return ((HASH_UNIVERSAL_A * result
                + HASH_UNIVERSAL_B)
                    % HASH_UNIVERSAL_P)
                        % MaxKeys;
}